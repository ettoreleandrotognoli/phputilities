<?php

namespace reflection;

use \PHPUnit_Framework_TestCase;

class ReflectionClassTest extends PHPUnit_Framework_TestCase {

	public function testClassExists(){
		$this->assertTrue(class_exists('reflection\\ReflectionClass'));
		$reflectionClass = new ReflectionClass('ArrayObject');
	}

	/**
	* @dataProvider newInstanceWithMapArgsProvider
	*/
	public function testNewIntanceWithMapArgs($map,$default=NULL){
		$defaultArray = array('name'=>'ettore','default'=>'default','array'=>$default,'value'=>$default);
		$reflectionClass = new ReflectionClass('reflection\\ConstructorTest');
		$instance = $reflectionClass->newInstanceWithMapArgs($map,$default);
		$trueArray = array_merge($defaultArray,$map);
		$this->assertEquals($trueArray,(array)$instance);
	}

	public function newInstanceWithMapArgsProvider(){
		return array(
			array(array()),
			array(array('name'=>'Ettore','value'=>true,'array'=>-1)),
			array(array('name'=>'Tognoli','value'=>true,'array'=>-1,'default'=>'teste'),'anyValue'),
			array(array('name'=>'','value'=>'','array'=>'','default'=>''),'anyValue'),
			array(array('name'=>0,'value'=>0,'array'=>0,'default'=>0),'anyValue'),
			array(array(),'notSetValue'),
		);
	}

}

class ConstructorTest{

	public $name;
	public $value;
	public $array;
	public $default;

	public function __construct($name='ettore',$value,$array,$default='default'){
		$this->name = $name;
		$this->value = $value;
		$this->array = $array;
		$this->default = $default;
	}

}
