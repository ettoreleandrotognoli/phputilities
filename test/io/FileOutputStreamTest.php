<?php
namespace io;

use \PHPUnit_Framework_TestCase;

class FileOutputStreamTest extends PHPUnit_Framework_TestCase{

	public function testClassExists(){
		$this->assertTrue(class_exists('io\\FileOutputStream'));
	}

	public function testInit(){
		$output = new FileOutputStream();
	}


	/**
	* @dataProvider randomTextProvider
	*/
	public function testWrite($text){
		$fileName = '/tmp/__phpunit__FileOutputStream.txt';
		$outputStream = new FileOutputStream($fileName);
		$outputStream->put($text);	
		$this->assertEquals($text,file_get_contents($fileName));
	}

	/**
	* @dataProvider randomTextProvider
	*/
	public function testAppendWrite($text){
		$fileName = '/tmp/__phpunit__FileOutputStream__WithAppend.txt';
		system('rm -f '.$fileName);
		for($i=0;$i<strlen($text);$i++){
			$outputStream = new FileOutputStream($fileName,true);	
			$outputStream->put($text[$i]);	
		}
		$this->assertEquals($text,file_get_contents($fileName));
		system('rm '.$fileName);
	}

	public function randomTextProvider(){
		$data = array();
		for($i=0;$i<20;$i++){
			$text = md5(rand());
			$data[] = array($text);
		}
		return $data;
	}
}