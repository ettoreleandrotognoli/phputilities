<?php

namespace io;

use \PHPUnit_Framework_TestCase;

class FileInputStreamTest extends PHPUnit_Framework_TestCase {

	public function testClassExists(){
		$this->assertTrue(class_exists('io\\FileInputStream'));
	}

	/**
	* @dataProvider getProvider
	*/
	public function testGet($text){
		$fileName = '/tmp/__phpunit__FileInputStreamTest__testGet.txt';
		system('echo "'.$text.'" > '.$fileName);
		$input = new FileInputStream($fileName);
		$this->assertEquals($text."\n",$input->get());
		system('rm -f '.$fileName);
	}

	public function getProvider(){
		return array(
			array('ettore leandro tognoli'),
			array(md5('ettore').md5('leandro').md5('tognoli')),
		);
	}

	/**
	* @dataProvider getfProvider
	*/
	public function testGetf($text,$format,$values){
		$fileName = '/tmp/__phpunit__FileInputStreamTest__testGet.txt';
		system('echo "'.$text.'" > '.$fileName);
		$input = new FileInputStream($fileName);
		$this->assertEquals($values,$input->getf($format));
		system('rm -f '.$fileName);
	}

	public function getfProvider(){
		return array(
			array('ettore leandro tognoli',"%s %s %s\n",array('ettore','leandro','tognoli')),
			array('29/11/1991',"%i/%i/%i\n",array(29,11,1991)),
		);
	}
}