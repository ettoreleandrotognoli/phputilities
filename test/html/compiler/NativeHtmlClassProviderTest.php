<?php

namespace html\compiler;

use \PHPUnit_Framework_TestCase;

class NativeHtmlClassProviderTest extends PHPUnit_Framework_TestCase {

	public function testClassExists(){
		$this->assertTrue(class_exists('html\\compiler\\NativeHtmlClassProvider'));
	}

	public function testSingleton(){
		$instance = NativeHtmlClassProvider::getInstance();
		$this->assertSame($instance,NativeHtmlClassProvider::getInstance());
	}

	/**
	* @dataProvider getHtmlClassProvider
	*/
	public function testGetHtmlClass($tag,$className){
		$this->assertEquals($className,NativeHtmlClassProvider::getInstance()->getHtmlClass($tag));
	}

	public function getHtmlClassProvider(){
		return array(
			array('div','html\\elements\\DefaultHtmlElement'),
			array('map','html\\elements\\DefaultHtmlElement'),
			array('span','html\\elements\\DefaultHtmlElement'),
			array('h1','html\\elements\\DefaultHtmlElement'),
			array('textarea','html\\elements\\DefaultHtmlElement'),
			array('input','html\\elements\\EmptyHtmlElement'),
			array('area','html\\elements\\EmptyHtmlElement'),
			array('script','html\\elements\\LiteralContentHtmlElement'),
			array('style','html\\elements\\LiteralContentHtmlElement'),
		);
	}

	public function testAddHtmlClassElement(){
		$provider = NativeHtmlClassProvider::getInstance();
		$provider->addHtmlClassElement('^.*$','agora vai ser aqui');
		$this->assertEquals('agora vai ser aqui',$provider->getHtmlClass('teste'));
		$provider->reboot();
	}

	public function testAddHtmlClassElements(){
		$provider = NativeHtmlClassProvider::getInstance();
		$provider->addHtmlClassElements(array('^.*$'=>'agora vai ser aqui'));
		$this->assertEquals('agora vai ser aqui',$provider->getHtmlClass('teste'));	
		$provider->reboot();
	}

}