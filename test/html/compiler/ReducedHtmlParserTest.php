<?php

namespace html\compiler;

use \PHPUnit_Framework_TestCase;
use io\FileInputStream;
use io\FileOutputStream;

class ReducedHtmlParserTest extends PHPUnit_Framework_TestCase{

	public function testClassExists(){
		$this->assertTrue(class_exists('html\\compiler\\ReducedHtmlParser'));
	}

	public function testMatchLine(){
		$parser = new ReducedHtmlParser();
		//var_dump($parser->matchLine('	html>body[id=body].content>h1>"""HelloWorld!!!'));
	}

	public function testMatchInline(){
		$parser = new ReducedHtmlParser();
		//var_dump($parser->matchInlineHtml('html>body[id=body][class=c1 c2].content>h1>'));	
	}

	public function testParseLine(){
		$parser = new ReducedHtmlParser();
		//var_dump($parser->parseLine('	html>body[id=body].content>h1>"""HelloWorld!!!'));	
	}

	public function testParse(){
		$parser = new ReducedHtmlParser();
		$inputFile = __DIR__.'/../template.rthtml';
		$expectedFile = __DIR__.'/../template.html';
		$outputFile = '/tmp/__phpunit__testParse.html';
		$result = $parser->parse(new FileInputStream($inputFile));			
		$out = new FileOutputStream($outputFile);
		foreach ($result as $html) {
			if(is_string($html)){
				$out->put($html,"\n");
				continue;
			}
			$html->render($out);
		}
		$this->assertFileEquals($expectedFile,$outputFile);
	}



}