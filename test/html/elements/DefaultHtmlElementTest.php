<?php

namespace html\elements;

use \PHPUnit_Framework_TestCase;

use reflection\ReflectionClass;

class DefaultHtmlElementTest extends PHPUnit_Framework_TestCase {

	public function testClassExists(){
		$this->assertTrue(class_exists('html\\elements\\DefaultHtmlElement'));
	}

	public function testAttr(){
		$htmlElement = new DefaultHtmlElement();
		$array = array();
		for($i=0;$i<100;$i++){
			$name = 'md5'.md5(rand().time());
			$value = rand();
			$array[$name] = $value;
		}
		foreach ($array as $key => $value) {
			$htmlElement->$key = $value;
		}
		foreach ($array as $key => $value) {
			$this->assertEquals($value,$htmlElement->$key);
		}
		$this->assertEquals(array_keys($array),$htmlElement->listAttributes());
	}


	/**
	* @dataProvider toStringProvider
	*/
	public function testToString($expected,$args){
		$reflection = new ReflectionClass('html\\elements\\DefaultHtmlElement');
		$html = $reflection->newInstanceArgs($args);
		$this->assertEquals($expected,(string)$html);
	}

	public function toStringProvider(){
		return array(
			array("<div>\n</div>\n",array()),
			array("<div>\n\tA 'quote' is &lt;b&gt;bold&lt;/b&gt;\n</div>\n",array('div',array(),'A \'quote\' is <b>bold</b>')),
			array("<div attr=\"teste\" >\n</div>\n",array('div',array('attr'=>'teste'))),
			array("<h1 attr=\"teste\" >\n\tettore\n</h1>\n",array('h1',array('attr'=>'teste'),'ettore')),
			array(
				"<h1 attr=\"teste\" id=\"identificador\" class=\"classe1 classe2\" >\n\tettore\n\tleandro\n\ttognoli\n</h1>\n",
				array('h1',
					array('attr'=>'teste','id'=>'identificador','class'=>array('classe1','classe2')),
					array('ettore','leandro','tognoli')
				)
			),
			array(
				"<h1>\n\t<span id=\"span1\" >\n\t\tEttore Leandro Tognoli\n\t</span>\n</h1>\n",
				array('h1',array(),new DefaultHtmlElement('span',array('id'=>'span1'),'Ettore Leandro Tognoli'))
			)
		);
	}

}