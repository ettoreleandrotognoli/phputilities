<?php

namespace html\elements;

use reflection\ReflectionClass;
use \PHPUnit_Framework_TestCase;

class EmptyHtmlElementTest extends PHPUnit_Framework_TestCase  {

	public function testClassExists(){
		$this->assertTrue(class_exists('html\\elements\\EmptyHtmlElement'));
	}

	/**
	* @dataProvider toStringProvider
	*/
	public function testToString($expected,$args){
		$reflection = new ReflectionClass('html\\elements\\EmptyHtmlElement');
		$html = $reflection->newInstanceArgs($args);
		$this->assertEquals($expected,(string)$html);
	}

	public function toStringProvider(){
		return array(
			array("<input/>\n",array()),
			array("<input name=\"teste\" />\n",array('input',array('name'=>'teste'))),
			array("<input name=\"inputName\" value=\"inputValue\" />\n",array('input',array('name'=>'inputName','value'=>'inputValue'))),
		);
	}


}