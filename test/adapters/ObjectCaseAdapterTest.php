<?php

namespace adapters;

use \ArrayObject;
use \PHPUnit_Framework_TestCase;

class ObjectCaseAdapterTest extends PHPUnit_Framework_TestCase {


	public function testClassExists(){
		$this->assertTrue(class_exists('adapters\\ObjectCaseAdapter'));
	}

	/**
	* @dataProvider invokeProvider
	*/
	public function testInvoke($result,$callback){
		$adapter = new ObjectCaseAdapter($callback);
		$this->assertEquals($result,$adapter());
	}

	public function invokeProvider(){
		return array(
			array(1,function(){return 1;}),
			array(1,new InvokeTestClass(1)),
		);
	}

	/**
	* @dataProvider invokeProviderWithArgs
	*/
	public function testInvokeWithArgs($result,$callback,$args){
		$adapter = new ObjectCaseAdapter($callback);
		$this->assertEquals($result,call_user_func_array($adapter,$args));
	}

	public function invokeProviderWithArgs(){
		return array(
			array(1,function(){return 1;},array()),
			array(1,new InvokeTestClass(1),array()),
			array(4,new InvokeSumTestClass(),array(2,2)),
			array(1+2+3+4,new InvokeSumTestClass(),array(1,2,3,4)),
		);
	}

	/**
	* @dataProvider callProvider
	*/
	public function testCall($expected,$object,$methodName){
		$adapter = new ObjectCaseAdapter($object);
		$this->assertEquals($expected,$adapter->$methodName());
	}
	
	public function callProvider(){
		return array(
			array(1,new CallTestClass(1),'methodName'),
			array(1,new CallTestClass(1),'method_name'),
			array(1,new CallTestClass(1),'METHOD_NAME'),
		);
	}

	/**
	* @dataProvider callProviderWithArgs
	*/
	public function testCallWithArgs($expected,$object,$methodName,$args){
		$adapter = new ObjectCaseAdapter($object);
		$this->assertEquals($expected,call_user_func_array(array($adapter,$methodName),$args));
	}
	
	public function callProviderWithArgs(){
		return array(
			array('a',new CallTestClass(1),'methodName',array('a')),
			array('abc',new CallTestClass(1),'method_name',array('a','b','c')),
			array('ettoreleandrotognoli',new CallTestClass(1),'METHOD_NAME',array('ettore','leandrotognoli')),
		);
	}

	/**
	* @dataProvider getAndSetProvider
	*/
	public function testGetAndSet($names,$value){
		$adapter = new ObjectCaseAdapter(new AttributeTestClass());
		foreach ($names as $name) {
			$this->assertEquals($value,$adapter->$name = $value);
		}
		foreach ($names as $name){
			$this->assertEquals($value,$adapter->$name);	
		}
	}

	public function getAndSetProvider(){
		return array(
			array(array('attributeName','AttributeName','ATTRIBUTE_NAME') , NULL),
			array(array('attributeName','AttributeName','ATTRIBUTE_NAME') , array()),
			array(array('HYFFEN-TEST','Hyffen-Test','hyffen-test') , 'AnyValue'),
		);
	}
}

class InvokeTestClass{

	public $value;

	public function __construct($value){
		$this->value = $value;
	}

	public function __invoke(){
		return $this->value;
	}

}

class InvokeSumTestClass{

	public function __invoke(){
		return array_sum(func_get_args());
	}

}

class CallTestClass{

	public $value;

	public function __construct($value){
		$this->value = $value;
	}

	public function methodName(){
		if(func_num_args() == 0)
			return $this->value;
		return implode(func_get_args());
	}

}

class AttributeTestClass extends ArrayObject{

	public function __set($name,$value){
		return $this[$name] = $value;
	}

	public function __get($name){
		return $this[$name];
	}

}
