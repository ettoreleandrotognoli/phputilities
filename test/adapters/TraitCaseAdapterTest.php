<?php

namespace adapters;

use \PHPUnit_Framework_TestCase;

class TraitCaseAdapterTest extends PHPUnit_Framework_TestCase {

	private $object;

	public function testTraitExists(){
		$this->assertTrue(trait_exists('adapters\\TraitCaseAdapter'));
	}

	/**
	* @dataProvider attributesProvider
	*/
	public function testAttributes($getName,$setName){
		$this->object = new TestClass();
		$value = rand();
		$this->object->$setName = $value;
		$this->assertEquals($value,$this->object->$getName);
	}

	public function attributesProvider(){
		return array(
			array('attributeName','attribute_name'),
			array('attributeName','ATTRIBUTE_NAME'),
			array('attributeName','ATTRIBUTE_NAME'),
			array('ATTRIBUTE-name','AttributeName'),
		);
	}

	/**
	* @dataProvider methodProvider
	*/
	public function testMethod($method){
		$this->object = new TestClass();
		$this->assertTrue($this->object->$method());
		$args = array();
		$size = rand(0,20);
		for($i=0;$i<$size;$i++)
			$args[] = rand();

	}

	public function methodProvider(){
		return array(
			array('methodName'),
			array('method_name'),
			array('method-name'),
			array('Method-Name'),
			array('Method_Name'),
			array('MethodName'),
		);
	}

	/**
	* @dataProvider methodWithArgsProvider
	*/
	public function testMethodWithArgs($method){
		$this->object = new TestClass();
		$args = array();
		$size = rand(0,20);
		for($i=0;$i<$size;$i++)
			$args[] = rand();
		$this->assertEquals($args,call_user_func_array(array($this->object,$method),$args));
	}

	public function methodWithArgsProvider(){
		return array(
			array('methodNameWithArgs'),
			array('method_name_with_args'),
			array('method-name-with-args'),
			array('Method-Name-With-Args'),
			array('Method_Name_With_Args'),
			array('MethodNameWithArgs'),
		);
	}




}

class TestClass{

	use TraitCaseAdapter;

	public $attributeName;

	public function methodName(){
		return true;
	}

	public function methodNameWithArgs(){
		return func_get_args();
	}

}
