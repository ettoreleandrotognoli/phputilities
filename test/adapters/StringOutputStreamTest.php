<?php

namespace adapters;

use \PHPUnit_Framework_TestCase;

class StringOutputStreamTest extends PHPUnit_Framework_TestCase {


	public function testClassExists(){
		$this->assertTrue(class_exists('adapters\\StringOutputStream'));
	}

	/**
	* @dataProvider putProvider
	*/
	public function testPut(){
		$stringStream = new StringOutputStream();
		foreach (func_get_args() as $arg) {
			$stringStream->put($arg);
		}
		$this->assertEquals(implode(func_get_args()),(string)$stringStream);
		$stringStream = new StringOutputStream();
		call_user_func_array(array($stringStream,'put'),func_get_args());
		$this->assertEquals(implode(func_get_args()),(string)$stringStream);
	}


	public function putProvider(){
		return array(
			array('teste'),
			array('teste','testando','mais um teste'),
			array('ettore','leandro','tognoli'),
		);
	}

	/**
	* @dataProvider putfProvider
	*/
	public function testPutf($array){
		$stringStream = new StringOutputStream();
		$result = '';
		foreach (func_get_args() as $arg) {
			$result .= call_user_func_array('sprintf',$arg);
			call_user_func_array(array($stringStream,'putf'),$arg);
		}
		$this->assertEquals($result,(string)$stringStream);
	}


	public function putfProvider(){
		return array(
			array(array('1'),array('2'),array('3')),
			array(array('%i',1),array('%i',2),array('%i',3))
		);
	}

}
