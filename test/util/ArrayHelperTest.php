<?php


namespace util;

use \PHPUnit_Framework_TestCase;

class ArrayHelperTest extends PHPUnit_Framework_TestCase {

	public function testClassExists(){
		$this->assertTrue(class_exists('util\\ArrayHelper'));
	}

	/**
	* @expectedException \Exception
	*/
	public function testExcpectException(){
		throw new \Exception();
	}


	public function testGetIfSet(){
		$array = array();
		$this->assertNull(ArrayHelper::getIfSet($array,'Undefined index'));
		$this->assertFalse(ArrayHelper::getIfSet($array,'Undefined index',false));
		$this->assertTrue(ArrayHelper::getIfSet($array,'Undefined index',true));
		$default = 'default';
		$this->assertEquals($default,ArrayHelper::getIfSet($array,'Undefined index',$default));
	}

	/**
	* @dataProvider dataAndObejctArrayProvider
	*/
	public function testDataArrayToObjectArray($dataArray,$objectArray){
		$this->assertEquals($objectArray,ArrayHelper::dataArrayToObjectArray($dataArray));
	}

	/**
	* @dataProvider dataAndObejctArrayProvider
	*/
	public function testDataArrayToObjectArrayWithTrueParameter($dataArray,$objectArray){
		foreach ($objectArray as $key => $value) {
			$objectArray[$key] = (object)$value;
		}
		$this->assertEquals($objectArray,ArrayHelper::dataArrayToObjectArray($dataArray,true));
	}

	/**
	* @dataProvider dataAndObejctArrayProvider
	*/
	public function testObjectArrayToDataArray($dataArray,$objectArray){
		$this->assertEquals($dataArray,ArrayHelper::objectArrayToDataArray($objectArray));
	}

	public function dataAndObejctArrayProvider(){
		$dataArray = array(
			'id' => [1,2,3],
			'name' => ['ettore','barbara','otavio'],
		);
		$objectArray = array(
			array(
				'id' => 1,
				'name' => 'ettore',
			),
			array(
				'id' => 2,
				'name' => 'barbara',
			),
			array(
				'id' => 3,
				'name' => 'otavio',
			),
		);
		return array(
			array($dataArray,$objectArray),
		);
	}
}
