<?php

namespace util\structure;

use \PHPUnit_Framework_TestCase;

class StackTest extends PHPUnit_Framework_TestCase {

	public function testClassExists(){
		$this->assertTrue(class_exists('util\\structure\\Stack'));
	}


	/**
	* @dataProvider arrayProvider
	*/
	public function testStack($array){
		$stack = new Stack();
		foreach ($array as $item) {
			$stack->push($item);
		}
		$reverse = array();
		while(!$stack->isEmpty()){
			$reverse[] = $stack->pop();
		}
		$this->assertEquals(array_reverse($array),$reverse);
	}

	public function arrayProvider(){
		$return = array();
		for($i=0;$i<10;$i++){
			$array = range(0,10);
			shuffle($array);
			$return[] = array($array);
		}
		return $return;
	}


	public function testStackWithRef(){
		$stack = new Stack();
		$a1 = array(1,2,3);
		$a2 = array('a','b','c');
		$a3 = array();
		$a4 = new \stdClass();
		$array = array($a1,$a2,$a3,$a4);
		foreach ($array as $item) {
			$stack->push($item);
		}
		$a3[] = array_pop($a1);
		$a3[] = end($a1);
		$a3[] = array_shift($a2);
		$a4->teste = 'ettore';
		$reverse = array();
		while(!$stack->isEmpty()){
			$reverse[] = $stack->pop();
		}
		$this->assertEquals(array_reverse($array),$reverse);
	}

	/*public function testTop(){
		$stack = new Stack();
		$array = array('oi');
		$stack->push(array());
		$top = $stack->top();
		$top[] = 'oi';
		$this->assertEquals($array,$stack->pop());
	}*/

}