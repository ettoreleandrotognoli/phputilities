<?php

namespace util;

use \PHPUnit_Framework_TestCase;

class NameHelperTest extends PHPUnit_Framework_TestCase {


	/**
	* @dataProvider namesProvider
	*/
	public function testExplodeName($nameCases){
		foreach ($nameCases['original'] as  $name) {
			$this->assertEquals($nameCases['explode'],NameHelper::explodeName($name));
		}		
	}

	/**
	* @dataProvider namesProvider
	*/
	public function testToLowerCamelCase($nameCases){
		foreach ($nameCases['original'] as  $name)
			$this->assertEquals($nameCases['lowerCamelCase'],NameHelper::toLowerCamelCase($name));
		$this->assertEquals($nameCases['lowerCamelCase'],NameHelper::toLowerCamelCase($nameCases['explode']));
	}

	/**
	* @dataProvider namesProvider
	*/
	public function testToUpperCamelCase($nameCases){
		foreach ($nameCases['original'] as  $name)
			$this->assertEquals($nameCases['upperCamelCase'],NameHelper::toUpperCamelCase($name));
		$this->assertEquals($nameCases['upperCamelCase'],NameHelper::toUpperCamelCase($nameCases['explode']));
	}

	/**
	* @dataProvider namesProvider
	*/
	public function testToCapitalizedWithUnderscores($nameCases){
		foreach ($nameCases['original'] as  $name)
			$this->assertEquals($nameCases['capitalizedWithUnderscores'],NameHelper::toCapitalizedWithUnderscores($name));
		$this->assertEquals($nameCases['capitalizedWithUnderscores'],NameHelper::toCapitalizedWithUnderscores($nameCases['explode']));	
	}

	/**
	* @dataProvider namesProvider
	*/
	public function testToLowerCaseWithUnderscores($nameCases){
		foreach ($nameCases['original'] as  $name)
			$this->assertEquals($nameCases['lowerCaseWithUnderscores'],NameHelper::toLowerCaseWithUnderscores($name));
		$this->assertEquals($nameCases['lowerCaseWithUnderscores'],NameHelper::toLowerCaseWithUnderscores($nameCases['explode']));	
	}

	/**
	* @dataProvider namesProvider
	*/
	public function testToCapitalizedWithHyphens($nameCases){
		foreach ($nameCases['original'] as  $name)
			$this->assertEquals($nameCases['capitalizedWithHyphens'],NameHelper::toCapitalizedWithHyphens($name));
		$this->assertEquals($nameCases['capitalizedWithHyphens'],NameHelper::toCapitalizedWithHyphens($nameCases['explode']));	
	}

	/**
	* @dataProvider namesProvider
	*/
	public function testToLowerCaseWithHyphens($nameCases){
		foreach ($nameCases['original'] as  $name)
			$this->assertEquals($nameCases['lowerCaseWithHyphens'],NameHelper::toLowerCaseWithHyphens($name));
		$this->assertEquals($nameCases['lowerCaseWithHyphens'],NameHelper::toLowerCaseWithHyphens($nameCases['explode']));	
	}

	/**
	* @dataProvider customCaseProviderWithAllParamaters
	*/
	public function testToCustomCaseWithAllParameters($expected,$original,$case,$glue,$preffix,$suffix){
		$this->assertEquals($expected,NameHelper::toCustomCase($original,$case,$glue,$preffix,$suffix));
	}

	public function customCaseProviderWithAllParamaters(){
		return array(
			array('tbl_pessoa_fisica_sql','pessoaFisica',NameHelper::LOWER,'_','tbl_','_sql'),
			array('Pessoa-Fisica','pessoa_fisica',NameHelper::UCAMEL,'-','',''),
			array('tbl_PESSOA_FISICA_sql','pessoaFisica',NameHelper::UPPER,'_','tbl_','_sql'),
			array('pessoa-Fisica','pessoa_fisica',NameHelper::LCAMEL,'-','',''),
		);
	}

	/**
	* @dataProvider customCaseProviderWithMinParameters
	*/
	public function testToCustomCaseWithMinParameters($expected,$original,$case){
		$this->assertEquals($expected,NameHelper::toCustomCase($original,$case));
	}

	public function customCaseProviderWithMinParameters(){
		return array(
			array('pessoafisica','pessoaFisica',NameHelper::LOWER),
			array('PessoaFisica','pessoa_fisica',NameHelper::UCAMEL),
			array('pessoaFisica','PESSOA-FISICA',NameHelper::LCAMEL),
			array('PESSOAFISICA','PESSOA-FISICA',NameHelper::UPPER),
			array('pessoa','PESSOA',NameHelper::LCAMEL),
		);
	}

	/**
	* @dataProvider prepareArrayProvider
	*/
	public function testPrepareArray($expected,$original){
		$this->assertEquals($expected,NameHelper::prepareArray($original));
	}

	public function prepareArrayProvider(){
		return array(
			array(array(),array()),
			array(
				array('contentType'=>'application/json','token' =>md5('ettore')),
				array('Content-Type'=>'application/json','Token'=>md5('ettore'))
			),
			array(
				array('head'=>array('contentType'=>'application/json','token' =>md5('ettore')),'content'=>array()),
				array('head'=>array('Content-Type'=>'application/json','Token'=>md5('ettore')),'content'=>array())
			)
		);
	}

	public function namesProvider(){
		return array(
			array(array(
				'original' => array(
					'ettore-leandro-tognoli',
					'ETTORE_LeandroTognoli',
					'ETTORE_LEANDRO_TOGNOLI',
					'EttoreLeandroTognoli',
					'ettoreLeandroTognoli',
					'ettore leandro tognoli',
					'ettore_leandro_tognoli',
				),
				'lowerCamelCase' => 'ettoreLeandroTognoli',
				'upperCamelCase' => 'EttoreLeandroTognoli',
				'capitalizedWithUnderscores' => 'ETTORE_LEANDRO_TOGNOLI',
				'lowerCaseWithUnderscores' => 'ettore_leandro_tognoli',
				'capitalizedWithHyphens' => 'ETTORE-LEANDRO-TOGNOLI',
				'lowerCaseWithHyphens' => 'ettore-leandro-tognoli',
				'explode' => array('ettore','leandro','tognoli')
			)),
			array(array(
				'original' => array('test-with-numbers123'),
				'lowerCamelCase' => 'testWithNumbers123',
				'upperCamelCase' => 'TestWithNumbers123',
				'capitalizedWithUnderscores' => 'TEST_WITH_NUMBERS123',
				'lowerCaseWithUnderscores' => 'test_with_numbers123',
				'capitalizedWithHyphens' => 'TEST-WITH-NUMBERS123',
				'lowerCaseWithHyphens' => 'test-with-numbers123',
				'explode' => array('test','with','numbers123')
			)),
			array(array(
				'original' => array(''),
				'lowerCamelCase' => '',
				'upperCamelCase' => '',
				'capitalizedWithUnderscores' => '',
				'lowerCaseWithUnderscores' => '',
				'capitalizedWithHyphens' => '',
				'lowerCaseWithHyphens' => '',
				'explode' => array()
			)),
		);
	}
	
}