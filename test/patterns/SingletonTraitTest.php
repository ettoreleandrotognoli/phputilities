<?php

namespace patterns;

use \PHPUnit_Framework_TestCase;

class SingletonTraitTest extends PHPUnit_Framework_TestCase {

	public function testTraitExists(){
		$this->assertTrue(trait_exists('patterns\\SingletonTrait'));
	}

	public function testSingleton(){
		$firstInstance = SingletonTestClass::getInstance(); 
		for($i=0;$i<3;$i++){
			$instance= SingletonTestClass::getInstance(); 
			$this->assertTrue(isset($instance));
			$this->assertEquals(1,$instance->getCount());
			$this->assertSame($firstInstance,$instance);
		}
	}

	public function testMultipleSingleton(){
		$firstInstances = array(SingletonTestClass::getInstance(),OtherSingletonTestClass::getInstance());
		for($i=0;$i<3;$i++){
			$instances = array(SingletonTestClass::getInstance(),OtherSingletonTestClass::getInstance());
			foreach ($instances as $instance) {
				$this->assertTrue(isset($instance));
				$this->assertEquals(1,$instance->getCount());
			}
			$this->assertEquals($firstInstances,$instances);
		}
	}

}

class SingletonTestClass{

	use SingletonTrait;

	private static $count = 0;

	private function __construct(){
		self::$count++;
	}

	public function getCount(){
		return self::$count;
	}

}

class OtherSingletonTestClass{

	use SingletonTrait;

	private static $count = 0;

	private function __construct(){
		self::$count++;
	}

	public function getCount(){
		return self::$count;
	}

}