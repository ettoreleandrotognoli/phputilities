<?php

namespace util\structure;

class Stack {

	private $size = 0;
	private $content = array();

	public function pop(){
		$this->size--;
		return array_pop($this->content);
	}

	public function push($item){
		$this->content[] = $item;
		$this->size++;
	}

	public function isEmpty(){
		return ($this->size == 0);
	}

	public function getContent(){
		return $this->content;
	}

}