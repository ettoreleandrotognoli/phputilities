<?php

namespace util;

class NameHelper {

	const UPPER = 'U';
	const LOWER = 'l';
	const LCAMEL = 'c';
	const UCAMEL = 'C';

	public static function explodeName($string,$regex='@[\\- _]@'){
		$nameParts = array();
		$explode = preg_split($regex,$string,-1,PREG_SPLIT_NO_EMPTY);
		foreach ($explode as $part) {
			if(preg_match('@^[A-Z0-9]+$@',$part) || preg_match('@^[a-z0-9]+$@',$part)){
				$nameParts[] = strtolower($part);
				continue;
			}
			$matchs = array();
			preg_match_all('@(:?(^[a-z][a-z0-9]*|[A-Z][a-z0-9]*))@',$part,$matchs);
			foreach ($matchs[0] as $m) {
				$nameParts[] = strtolower($m);
			}
		}
		return $nameParts;
	}

	public static function toLowerCamelCase($mixed){
		if(!is_array($mixed))
			$mixed = NameHelper::explodeName($mixed);
		$name = '';
		foreach ($mixed as $namePart) {
			$name .= ucfirst(strtolower($namePart));
		}
		return lcfirst($name);
	}

	public static function toUpperCamelCase($mixed){
		if(!is_array($mixed))
			$mixed = NameHelper::explodeName($mixed);
		$name = '';
		foreach ($mixed as $namePart) {
			$name .= ucfirst(strtolower($namePart));
		}
		return $name;
	}

	public static function toCapitalizedWithUnderscores($mixed){
		if(!is_array($mixed))
			$mixed = NameHelper::explodeName($mixed);
		return implode('_',array_map('strtoupper',$mixed));
	}

	public static function toLowerCaseWithUnderscores($mixed){
		if(!is_array($mixed))
			$mixed = NameHelper::explodeName($mixed);
		return implode('_',array_map('strtolower',$mixed));
	}

	public static function toCapitalizedWithHyphens($mixed){
		if(!is_array($mixed))
			$mixed = NameHelper::explodeName($mixed);
		return implode('-',array_map('strtoupper',$mixed));	
	}

	public static function toLowerCaseWithHyphens($mixed){
		if(!is_array($mixed))
			$mixed = NameHelper::explodeName($mixed);
		return implode('-',array_map('strtolower',$mixed));
	}

	private static function arrayToCase($case,$array){
		switch ($case) {
			case NameHelper::LOWER:
				return array_map('strtolower',$array);
			case NameHelper::UPPER:
				return array_map('strtoupper',$array);
			case NameHelper::UCAMEL:
				return array_map(function($e){ return ucfirst(strtolower($e));},$array);
			case NameHelper::LCAMEL:
				$array = array_map(function($e){ return ucfirst(strtolower($e));},$array); 
				$array[0] = lcfirst($array[0]);
				return $array;
			default:
				return $array;
		}
	}

	public static function toCustomCase($mixed,$case,$glue='',$preffix='',$suffix=''){
		if(!is_array($mixed))
			$mixed = NameHelper::explodeName($mixed);
		return $preffix.implode($glue,NameHelper::arrayToCase($case,$mixed)).$suffix;
	}


	public static function prepareArray($array,$case=NameHelper::LCAMEL,$glue='',$preffix='',$suffix=''){
		$preparedArray = array();
		foreach ($array as $key => $value) {
			$newKey = $key;
			if(is_string($key))
				$newKey = NameHelper::toCustomCase($key,$case,$glue,$preffix,$suffix);
			$newValue = $value;
			if(is_array($value))
				$newValue = NameHelper::prepareArray($value,$case,$glue,$preffix,$suffix);
			$preparedArray[$newKey] = $newValue;
		}
		return $preparedArray;
	}
}