<?php

namespace util;

class ArrayHelper {

	public static function getIfSet($array,$key,$ifNotSetValue=null){
		if(isset($array[$key]))
			return $array[$key];
		return $ifNotSetValue;
	}

	public static function dataArrayToObjectArray($dataArray,$convertObject=false){
		$objectArray = array();
		$keys = array();
		$size = 0;
		foreach ($dataArray as $key => $array) {
			$currentSize = count($array);
			$size = $currentSize>$size?$currentSize:$size;
			$keys[] = $key;
		}
		for($i=0;$i<$size;$i++){
			$object = array();
			foreach ($keys as $key) {
				$object[$key] = $dataArray[$key][$i];
			}
			if(!$convertObject){
				$objectArray[] = $object;
				continue;
			}
			if($convertObject === true){
				$objectArray[] = (object)$object;
				continue;
			}
			throw new \InvalidArgumentException();
		}
		return $objectArray;
	}

	public static function objectArrayToDataArray($objectArray,$allKeys=null){
		if(empty($objectArray))
			return array();
		if($allKeys == null)
			$allKeys = array_keys($objectArray[0]);
		$dataArray = array();
		foreach ($allKeys as $key) {
			$dataArray[$key] = array();
		}
		foreach ($objectArray as $objectIndex => $object) {
			foreach($allKeys as $key){
				$value = ArrayHelper::getIfSet($object,$key);
				$dataArray[$key][$objectIndex] = $value;
			}
		}
		return $dataArray;
	}

}
