<?php

namespace patterns;

trait SingletonTrait{

	private static $instance;

	public static function getInstance(){
		if(empty(static::$instance)){
			$args = func_get_args();
			$reflectionClass = new \ReflectionClass(__CLASS__);
			$constructor = $reflectionClass->getConstructor();
			static::$instance = $reflectionClass->newInstanceWithoutConstructor();
			$constructor->setAccessible(true);
			$constructor->invokeArgs(static::$instance,$args);
			$constructor->setAccessible(false);
		}
		return static::$instance;
	}

	final private function __wakeup(){}

    final private function __clone() {}    

}