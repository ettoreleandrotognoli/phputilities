<?php

namespace adapters;

use util\NameHelper;

trait TraitCaseAdapter{

	public function __call($method,$args){
		$trueMethodName = NameHelper::toLowerCamelCase($method);
		return call_user_func_array(array($this,$trueMethodName),$args);
	}

	public function __get($attr){
		$trueAttrName = NameHelper::toLowerCamelCase($attr);
		return $this->$trueAttrName;
	}

	public function __set($attr,$value){
		$trueAttrName = NameHelper::toLowerCamelCase($attr);
		return $this->$trueAttrName = $value;
	}

	public function __isset($attr){
		$trueAttrName = NameHelper::toLowerCamelCase($attr);
		return isset($this->$attr);
	}

	public function __unset($attr){
		$trueAttrName = NameHelper::toLowerCamelCase($attr);
		unset($this->$attr);	
	}

}