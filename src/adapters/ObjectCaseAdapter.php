<?php

namespace adapters;

use util\NameHelper;

class ObjectCaseAdapter {

	private $__object__;

	public function __construct($__object__){
		$this->__object__ = $__object__;
	}

	public function __invoke(){
		return call_user_func_array($this->__object__,func_get_args());
	}

	public function __call($method,$args){
		$trueMethodName = NameHelper::toLowerCamelCase($method);
		return call_user_func_array(array($this->__object__,$trueMethodName),$args);
	}

	public function __get($attr){
		$trueAttrName = NameHelper::toLowerCamelCase($attr);
		return $this->__object__->$trueAttrName;
	}

	public function __set($attr,$value){
		$trueAttrName = NameHelper::toLowerCamelCase($attr);
		return $this->__object__->$trueAttrName = $value;
	}

	public function __isset($attr){
		$trueAttrName = NameHelper::toLowerCamelCase($attr);
		return isset($this->$attr);
	}

	public function __unset($attr){
		$trueAttrName = NameHelper::toLowerCamelCase($attr);
		unset($this->$attr);	
	}

}