<?php

namespace adapters;

use io\OutputStream;

class StringOutputStream implements OutputStream{

	private $string;

	public function __construct(&$string = ""){
		$this->string = $string;
	}

	public function put(){
		foreach(func_get_args() as $arg){
			$this->string .= (string)$arg;
		}
	}

	public function putf($format){
		$this->string .= call_user_func_array('sprintf',func_get_args());
	}

	public function __toString(){
		return $this->string;
	}

}