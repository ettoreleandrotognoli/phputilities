<?php

namespace html\compiler;

use io\InputStream;
use util\ArrayHelper;
use reflection\ReflectionClass;

class ReducedHtmlParser{

	//const LINE_REGEX = "@^\t*(?P<html>(?:[^> \t\"]+>?)*)(?:\"\"\"(?P<string>.*))?$@";
	const LINE_REGEX = "@^\t*(?P<html>[^\"]*)(?:\"\"\"(?P<string>.*))?$@";
	//const HTML_REGEX = '@(?P<tag>[a-zA-Z0-9_-]+)(?P<attr>(\[[a-zA-Z0-9_-]+=[a-zA-Z0-9_ /\.-]+\])*)(?P<class>((\.[a-zA-Z0-9_-]+)*))@';
	const HTML_REGEX = '@(?P<tag>[a-zA-Z0-9_-]+)(?P<attr>(\[[a-zA-Z0-9_-]+=.+\])*)(?P<class>((\.[a-zA-Z0-9_-]+)*))@';

	private $classProviders;

	public function __construct(HtmlClassProvider $htmlClassProvider=null){
		if(empty($htmlClassProvider))
			$htmlClassProvider = NativeHtmlClassProvider::getInstance();
		$this->classProviders = array($htmlClassProvider);
	}

	public function addHtmlClassProvider(HtmlClassProvider $htmlClassProvider){
		array_unshift($this->classProviders,$htmlClassProvider);
	}

	public function matchLine($line){
		$matches = array();
		if(!preg_match(self::LINE_REGEX,$line,$matches))
			throw new \Exception();
		$literalString = ArrayHelper::getIfSet($matches,'string',false);
		$inlineHtml = ArrayHelper::getIfSet($matches,'html',false);
		return array($inlineHtml,$literalString);
	}

	public function matchInlineHtml($inlineHtml){
		$elements = array();
		$htmls = preg_split('@>@',$inlineHtml,-1,PREG_SPLIT_NO_EMPTY);
		foreach ($htmls as $html) {
			$matches = array();
			if(!preg_match(self::HTML_REGEX,trim($html),$matches))
				throw new \Exception();
			$tag = ArrayHelper::getIfSet($matches,'tag',false);
			$attr = ArrayHelper::getIfSet($matches,'attr',array());
			$class = ArrayHelper::getIfSet($matches,'class','');
			$elements[] = array($tag,$attr,$class);
		}
		return $elements;
	}

	public function parse(InputStream $input){
		$htmlBuilder = new HtmlTreeBuilder();
		$currrentTab = 0;
		while($line = $input->get()){
			if(empty(trim($line)))
				continue;
			$tabs = $this->countTabs($line);
			while($tabs < $currrentTab){
				$htmlBuilder->closeInner();
				$currrentTab--;
			}
			while($tabs > $currrentTab){
				$htmlBuilder->openInner();
				$currrentTab++;
			}
			$parsedLine = $this->parseLine($line);
			$htmlBuilder->addElement($parsedLine);
		}
		while($currrentTab > 0){
			$htmlBuilder->closeInner();
			$currrentTab--;
		}
		return $htmlBuilder->build();
	}

	public function parseLine($line){
		list($inlineHtml,$literalString) = $this->matchLine($line);
		$htmls = $this->matchInlineHtml($inlineHtml);
		$lastElement = null;
		$parsedElements = array();
		foreach ($htmls as $html) {
			list($tag,$attr,$classes) = $html;
			$attr = preg_split('@[\]\[]+@',$attr,-1,PREG_SPLIT_NO_EMPTY);
			$attributes = array();
			foreach ($attr as $keyEqualValue) {
				list($key,$value) = explode('=',$keyEqualValue);
				$attributes[$key] = explode(' ',$value);
			}
			$classes = preg_split('@\.@',$classes,-1,PREG_SPLIT_NO_EMPTY); 
			$attributes['class'] = array_merge($classes,ArrayHelper::getIfSet($attributes,'class',array()));
			if(empty($attributes['class']))
				unset($attributes['class']);
			$newElement = $this->buildElement($tag,$attributes);
			if($lastElement)
				$lastElement->setContent($newElement);
			$parsedElements[] = $newElement;
			$lastElement = $newElement;
		}
		if($literalString !== false){
			if($lastElement)
				$lastElement->setContent($literalString);
			else
				$parsedElements[] = $literalString;
		}
		return $parsedElements;
	}

	public function buildElement($tag,$attrs){
		foreach ($this->classProviders as $provider) {
			$className = $provider->getHtmlClass($tag);
			if(!class_exists($className))
				continue;
			$reflectionClass = new ReflectionClass($className);
			$htmlElement = $reflectionClass->newInstance($tag,$attrs);
			return $htmlElement;
		}
		throw new \Exception('Tag not Supported ('.$tag.')');
	}

	protected function countTabs($string){
		$i=0;
		for($i=0;$string[$i] == "\t";$i++){

		}
		return $i;
	}

	protected function isBlank($string){
		return preg_match('@^[ \t]*$@',$string)?true:false;
	}


}