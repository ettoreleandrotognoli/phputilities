<?php


namespace html\compiler;

use util\structure\Stack;


class HtmlTreeBuilder{

	private $stack;


	public function __construct(HtmlClassProvider $htmlClassProvider=null){
		$this->stack = new Stack();
		$this->stack->push(array());
	}

	public function addElement($element){
		$top = $this->stack->pop();
		$top[] = $element;
		$this->stack->push($top);
	}

	public function openInner(){
		$this->stack->push(array());
	}

	public function closeInner(){
		$toClose = $this->stack->pop();
		$top = $this->stack->pop();
		$topInnerElement = end($top);
		if(is_array($topInnerElement)){
			$topInnerElement = end($topInnerElement);
		}
		foreach ($toClose as $innerElement) {
			if(is_array($innerElement)){
				$topInnerElement->addContent($innerElement[0]);
				continue;
			}
			$topInnerElement->addContent($innerElement);
		}
		$this->stack->push($top);
	}

	public function build(){
		$result = array();
		$content = $this->stack->getContent();
		return array_map(function($elements){
			if(is_array($elements))
				return $elements[0];
			return $elements;
		},$content[0]);
	}

}