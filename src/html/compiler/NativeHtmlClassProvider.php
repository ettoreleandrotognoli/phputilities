<?php

namespace html\compiler;

use patterns\SingletonTrait;
use util\NameHelper;

class NativeHtmlClassProvider implements HtmlClassProvider{

	use SingletonTrait;

	private function __construct(){

	}

	private $htmlClasses = array(
		'^(script|style)$' => 'html\\elements\\LiteralContentHtmlElement',
		'^(img|area|input|meta|link)$' => 'html\\elements\\EmptyHtmlElement',
		'^.*$' => 'html\\elements\\DefaultHtmlElement',
	);

	private $cache = array();

	public function getHtmlClass($tagName){
		if(isset($this->cache[$tagName]))
			return $this->cache[$tagName];
		$className = NameHelper::toUpperCamelCase($tagName);
		$className = 'html\\elements\\'.$className;
		if(class_exists($className)){
			$this->cache[$tagName] = $className;
			return $className;
		}
		foreach ($this->htmlClasses as $regex => $className) {
			if(!preg_match('@'.$regex.'@',$tagName))
				continue;
			$this->cache[$tagName] = $className;
			return $className;
		}
		throw new \Exception();
	}

	public function reboot(){
		self::$instance = new NativeHtmlClassProvider();
	}

	public function addHtmlClassElements(Array $classes){
		$this->htmlClasses = array_merge($classes,$this->htmlClasses,$classes);
		if(!empty($this->cache))
			$this->cache = array();
	}

	public function addHtmlClassElement($regex,$className){
		$this->htmlClasses = array_merge(array($regex=>$className),$this->htmlClasses,array($regex=>$className));
		if(!empty($this->cache))
			$this->cache = array();
	}

}