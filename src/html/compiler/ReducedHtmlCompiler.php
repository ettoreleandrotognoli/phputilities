<?php

namespace html\compiler;

use io\OutputStream;
use io\InputStream;

use util\ArrayHelper;
use reflection\ReflectionClass;

class ReducedHtmlCompiler{

	private $stack = array();
	private $classProviders;

	public function __construct(HtmlClassProvider $htmlClassProvider=null){
		if(empty($htmlClassProvider))
			$htmlClassProvider = NativeHtmlClassProvider::getInstance();
		$this->classProviders = array($htmlClassProvider);
	}

	public function addHtmlClassProvider(HtmlClassProvider $htmlClassProvider){
		array_unshift($this->classProviders,$htmlClassProvider);
	}

	public function compile(InputStream $input,OutputStream $output){
		$stackSize = count($this->stack);
		while($line = $input->get()){
			$tabs = $this->countTabs($line);
			$htmlLine = $this->buildInlineElements(trim($line));
			//var_dump($htmlLine);
			if(is_string($htmlLine[0])){
				$output->put(str_repeat("\t",$tabs).htmlentities($htmlLine[0])."\n");
			}
			else{
				$htmlLine[0]->render($output,$tabs);	
			}
		}
	}

	protected function buildInlineElements($line){
		$htmlAndString = explode('"""',$line);
		//var_dump($htmlAndString);
		$elements = explode('>',$htmlAndString[0]);
		//var_dump($elements);
		$htmlElements = array();
		foreach ($elements as $inlineElement) {
			if(empty($inlineElement))
				continue;
			$lastElement = end($htmlElements);
			$newElement = $this->buildInlineElement($inlineElement);
			$htmlElements[] = $newElement;
			if($lastElement)
				$lastElement->setContent($newElement);
		}
		$lastElement = end($htmlElements);
		$literal = ArrayHelper::getIfSet($htmlAndString,1,null);
		$htmlElements[] = $literal;
		if($lastElement)
			$lastElement->setContent($literal);
		return $htmlElements;
	}

	protected function buildInlineElement($inlineElement){
		$explode = explode('.',$inlineElement);
		$tagAndAttr = array_shift($explode);
		$classes = $explode;
		$split = preg_split('@[\\]\\[]+@',$tagAndAttr,-1);
		$tag = array_shift($split);
		$attrs = array();
		foreach ($split as $attr) {
			if(empty($attr))
				continue;
			list($attrName,$attrValue) = explode('=',$attr);
			$attrs[$attrName] = $attrValue;
		}
		if(!empty($classes)){
			$attrClass = ArrayHelper::getIfSet($attrs,'class',array());
			if(!is_array($attrClass))
				$attrClass = array($attrClass);
			$attrs['class'] = array_merge($classes,$attrClass);
		}
		return $this->buildElement($tag,$attrs);
	}

	public function buildElement($tag,$attrs){
		foreach ($this->classProviders as $provider) {
			$className = $provider->getHtmlClass($tag);
			if(!class_exists($className))
				continue;
			$reflectionClass = new ReflectionClass($className);
			$htmlElement = $reflectionClass->newInstance($tag,$attrs);
			return $htmlElement;
		}
		throw new \Exception('Tag not Supported ('.$tag.')');
	}

	protected function push($line){

	}

	protected function pop($line){

	}

	protected function countTabs($string){
		$i=0;
		for($i=0;$string[$i] == "\t";$i++){

		}
		return $i;
	}

}