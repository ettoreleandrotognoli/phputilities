<?php

namespace html\compiler;

interface HtmlClassProvider{
	
	public function getHtmlClass($tagName);

}