<?php

namespace html\elements;

use io\OutputStream;

interface HtmlElement{

	public function getTag();

	public function setTag($tag);

	public function getContent();

	public function setContent($content);

	public function addContent($html);
	
	public function render(OutputStream $output,$indentation);

	public function listAttributes();

	public function copy();

}