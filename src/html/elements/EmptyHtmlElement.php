<?php

namespace html\elements;

use io\OutputStream;

class EmptyHtmlElement extends AbstractHtmlElement{

	public function __construct($tag='input',$attr=array()){
		parent::__construct($tag,$attr);
	}

	public function setContent($content){
		
	}

	public function addContent($html){

	}

	public function getContent(){
		return null;
	}

	public function render(OutputStream $output,$indentation=0){
		$tabs = str_repeat("\t",$indentation);
		$output->put($tabs,'<',$this->tag);
		parent::renderAttr($output);
		$output->put("/>\n");
	}

	public function copy(){
		$reflection = new \Reflection(get_class());
		return $reflection->newInstance(
			$this->tag,
			$this->attr
		);
	}

}