<?php

namespace html\elements;

use io\OutputStream;

class DefaultHtmlElement extends AbstractHtmlElement{

	protected $content;
	
	public function __construct($tag='div',$attr=array(),$content=array()){
		parent::__construct($tag,$attr);
		$this->setContent($content);
	}

	public function setContent($content){
		$this->content = $content;
	}

	public function addContent($html){
		if(!is_array($this->content)){
			$this->content = array($this->content);
		}
		if(is_array($html)){
			$this->content = array_merge($this->content,$html);
			return;
		}
		$this->content[] = $html;
	}

	public function getContent(){
		return $this->content;
	}

	public function render(OutputStream $output,$indentation=0){
		$tabs = str_repeat("\t",$indentation);
		$output->put($tabs,'<',$this->tag);
		parent::renderAttr($output);
		$output->put(">\n");
		$this->renderContent($output,$indentation+1);
		$output->put($tabs,'</',$this->tag,">\n");
	}

	protected function renderContent(OutputStream $output,$indentation){
		$content = $this->prepareContent($this->content);
		if(!is_array($content))
			$content = array($content);
		foreach ($content as $element) {
			$this->renderElement($output,$element,$indentation);
		}
	}

	protected function prepareContent($content){
		if(is_null($content))
			return null;
		if(is_a($content,'html\\elements\\HtmlElement'))
			return $content;
		if(is_numeric($content) || is_object($content))
			$content = (string)$content;
		if(is_string($content))
			return htmlentities($content);
		if(is_array($content))
			return array_map(array($this,'prepareContent'),$content);
		throw new \Exception('Content not Supported');
	}

	protected function renderElement(OutputStream $output,$element,$indentation){
		if(empty($element))
			return;
		if(is_string($element)){
			$tabs = str_repeat("\t",$indentation);
			$output->put($tabs,$element,"\n");
			return;
		}
		if(is_a($element,'html\\elements\\HtmlElement')){
			$element->render($output,$indentation);
			return;
		}
		throw new \Exception();
	}

	public function copy(){
		$reflection = new \ReflectionClass(get_class());
		return $reflection->newInstance(
			$this->tag,
			$this->attr,
			$this->cloneContent($this->content)
		);
	}

	protected function cloneContent($content){
		if(is_null($content))
			return null;
		if(is_a($content,'html\\elements\\HtmlElement'))
			return $content->copy();
		if(is_numeric($content) || is_object($content))
			$content = (string)$content;
		if(is_string($content))
			return $content;
		if(is_array($content))
			return array_map(array($this,'cloneContent'),$content);
		throw new \Exception('Content not Supported');	
	}
}
