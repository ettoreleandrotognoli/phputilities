<?php

namespace html\elements;

class LiteralContentHtmlElement extends DefaultHtmlElement{

	/**
	* @Override
	*/
	protected function prepareContent($content){
		if(is_null($content))
			return null;
		if(is_a($content,'html\\elements\\HtmlElement'))
			return $content;
		if(is_numeric($content) || is_object($content))
			$content = (string)$content;
		if(is_string($content))
			return $content;
		if(is_array($content))
			return array_map(array($this,'prepareContent'),$content);
		throw new \Exception('Content not Supported');
	}

}