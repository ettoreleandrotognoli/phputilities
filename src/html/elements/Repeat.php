<?php

namespace html\elements;

use io\OutputStream;

class Repeat extends DefaultHtmlElement{

	private $replace = null;

	private static $defaultAttributes = array(
		'range' => array(0,1,1),
		'wildcard' => array('*')
	);

	public function __construct($tag='',$attr=array(),$content=null){
		parent::__construct($tag,$attr,$content);
		$this->attr = array_merge(self::$defaultAttributes,$this->attr);
	}

	public function render(OutputStream $output,$indentation=0){
		for($i=$this->range[0];$i<=$this->range[1];$i+=$this->range[2]){
			$this->replace = $i;
			$this->renderContent($output,$indentation);
		}
		$this->replace = null;
	}

	protected function renderContent(OutputStream $output,$indentation){
		$content = $this->prepareContent($this->content);
		if(!is_array($content))
			$content = array($content);
		foreach ($content as $element) {
			$this->renderElement($output,$element,$indentation);
		}
	}

	protected function prepareContent($content){
		if(is_null($content))
			return null;
		if(is_a($content,'html\\elements\\HtmlElement'))		
			return $this->replaceElement($content->copy());
		if(is_numeric($content) || is_object($content))
			$content = (string)$content;
		if(is_string($content)){
			foreach ($this->wildcard as $search) {
				$content = str_replace($search,$this->replace,$content);
			}
			return $content;
		}
		if(is_array($content))
			return array_map(array($this,'prepareContent'),$content);
		throw new \Exception('Content not Supported');
	}

	protected function replaceElement($element){
		foreach ($element->listAttributes() as $name) {
			$value = $element->$name;
			$newValue = array();
			if(!is_array($value))
				$value = array($value);
			foreach ($value as $toReplace) {
				foreach ($this->wildcard as $search) {
					$newValue[] = str_replace($search,$this->replace,$toReplace);
				}
			}
			$element->$name = $newValue;
		}
		$content = $element->getContent();
		$content = $this->prepareContent($content);
		$element->setContent($content);
		return $element;
	}
	
}