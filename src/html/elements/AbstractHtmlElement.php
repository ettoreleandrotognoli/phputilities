<?php

namespace html\elements;

use adapters\StringOutputStream;
use io\OutputStream;

abstract class AbstractHtmlElement implements HtmlElement{

	protected $tag;
	protected $attr;

	public function __construct($tag,$attr=array()){
		$this->tag = $tag;
		$this->attr = $attr;
	}

	public function __set($name,$value){
		return $this->attr[$name] = $value;
	}

	public function __get($name){
		return $this->attr[$name];
	}

	public function __isset($name){
		return isset($attr[$name]);
	}

	public function __unset($name){
		unset($attr[$name]);
	}

	protected function renderAttr(OutputStream $output){
		foreach ($this->attr as $key => $value) {
			if(is_array($value))
				$value = implode(' ',$value);
			$output->put(' ',$key,'="',$value,'"');
		}
		if(!empty($this->attr))
			$output->put(' ');
	}

	public function getTag(){
		return $this->tag;
	}

	public function setTag($tag){
		$this->$tag = $tag;
	}

	public function listAttributes(){
		return array_keys($this->attr);
	}

	public function __toString(){
		$stringStream = new StringOutputStream();
		$this->render($stringStream,0);
		return (string)$stringStream;
	}

	public abstract function getContent();

	public abstract function setContent($content);

	public abstract function render(OutputStream $output,$indentation);

	public abstract function copy();
	
}