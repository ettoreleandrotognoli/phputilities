<?php

namespace io;

interface InputStream{

	public function get();

	public function getf($format);
	
}