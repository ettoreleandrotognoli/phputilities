<?php

namespace io;

class FileOutputStream implements OutputStream{

	private $file;
	
	public function __construct($fileName = 'php://output',$append=false){
		$this->file = fopen($fileName,$append?'a+':'w+');
	}

	public function __destruct(){
		fclose($this->file);
	}

	public function put(){
		foreach(func_get_args() as $arg){
			fwrite($this->file,$arg);
		}
	}

	public function putf($format){
		return call_user_func_array('fprintf',array_merge(array($this->file),func_get_args()));
	}

}