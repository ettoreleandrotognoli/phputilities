<?php

namespace io;

class FileInputStream implements InputStream{

	private $file;

	public function __construct($fileName){
		$this->file = fopen($fileName,"r");
	}

	public function __destruct(){
		fclose($this->file);
	}

	public function get(){
		return fgets($this->file);
	}

	public function getf($format){
		return fscanf($this->file,$format);
	}

}