<?php

namespace io;

interface OutputStream{
	
	public function put();

	public function putf($format);

}