<?php

namespace reflection;

use util\ArrayHelper;

class ReflectionClass extends \ReflectionClass {

	public function __construct($mixed){
		parent::__construct($mixed);
	}

	public function newInstanceWithMapArgs($mapArray,$notSetValue = null){
		$newInstance = parent::newInstanceWithoutConstructor();
		$this->invokeConstructorWithMapArgs($newInstance,$mapArray,$notSetValue);
		return $newInstance;
	}

	public function invokeConstructorWithMapArgs($instance,$mapArray,$notSetValue=null){
		$orderedValues = array();
		$reflectionConstructor = parent::getConstructor();
		foreach($reflectionConstructor->getParameters() as $parameter){
			try{
				$value = $parameter->getDefaultValue();
			}
			catch(\ReflectionException $ex){
				$value = $notSetValue;
			}
			$orderedValues[] = ArrayHelper::getIfSet($mapArray,$parameter->getName(),$value);
		}
		$reflectionConstructor->invokeArgs($instance,$orderedValues);
	}

	public function invokeConstructorWithArgs($instance,$args){
		$reflectionConstructor = parent::getConstructor();
		$reflectionConstructor->invokeArgs($instance,$args);
	}

}